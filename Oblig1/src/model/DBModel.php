<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
		    try {
                $this->db = new PDO('mysql:host=localhost;dbname=assignment1', 'root');
            }
            catch (PDOException $ex) {
                print $ex->getMessage();
            }
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $books = array();
        try {
            $booksArr = $this->db->query('SELECT * FROM book');
            $i = 0;
            foreach ($booksArr as $book) {
                    $books[$i++] = new Book(
                                                $book['title'],
                                                $book['author'],
                                                $book['description'],
                                                $book['id']
                    );
            }
        } catch (PDOException $ex) {
            print $ex->getMessage();
        }
        return $books;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        try {
            $statement = $this->db->prepare("SELECT * FROM book WHERE ID = ?");
            $statement->execute(array($id));
            $bookArr = $statement->fetch();
        } catch (PDOException $ex) {
            print $ex->getMessage();
        }
        $bookObj = null;

		if($bookArr != null) {
		    $bookObj = new Book($bookArr['title'], $bookArr['author'], $bookArr['description'], $bookArr['id']);
        }

		//print_r($bookArr);

        return $bookObj;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        try {
            $statement = $this->db->prepare("INSERT INTO book(title, author, description) VALUES(?, ?, ?)");
            $statement->execute(array($book->title, $book->author, $book->description));
            $book->id = $this->db->lastInsertId();
        } catch (PDOException $ex) {
            print $ex->getMessage();
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept. $book->id decides which book to update.
     */
    public function modifyBook($book)
    {
        try {
            $statement = $this->db->prepare("UPDATE book SET title=?, author=?, description=? WHERE id=?");
            $statement->execute(array($book->title, $book->author, $book->description, $book->id));
        } catch (PDOException $ex) {
            print $ex->getMessage();
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        try {
            $statement = $this->db->prepare('DELETE FROM book WHERE id=?');
            $statement->execute(array($id));
        } catch (PDOException $ex) {
            print $ex->getMessage();
        }
    }
	
}

?>